import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { Profile } from '../profile/profile';
import { Login } from '../login/login';
import { AuthData } from '../../providers/auth-data';

import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // Refs
  datas: FirebaseObjectObservable<any>;

  constructor(public nav: NavController, public af: AngularFire, public authData: AuthData,) {
    this.nav = nav;
    this.datas = this.af.database.object('/default-map/map');
    
  }

  goToProfile() {
    this.nav.push(Profile);
  }

  logOut(){
    this.authData.logoutUser().then(() => {
      this.nav.setRoot(Login);
    });
  }

}
